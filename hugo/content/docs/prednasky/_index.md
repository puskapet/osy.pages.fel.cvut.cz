---
title: "Přednášky"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: true
---

# Program přednášek
|     | Datum        | Téma                                     | Další materiály |
|-----|--------------|------------------------------------------|-----------------|
| 1.  | 24. 9. 2020  | [Úvod do operačních systémů][l1]         | [Příklady][p1]  |
| 2.  | 1. 10. 2020  | [Systémová volání][l2]                   | [Příklady][p2]  |
| 3.  | 8. 10. 2020  | [Procesy a vlákna][l3]                   | [Příklady][p3]  |
| 4.  | 15. 10. 2020 | [Synchronizace][l4]                      | [Příklady][p4]  |
| 5.  | 22. 10. 2020 | [Meziprocesní komunikace][l5]            | [Příklady][p5]  |
| 6.  | 29. 10. 2020 | [Virtuální paměť][l6]                    |                 |
| 7.  | 5. 10. 2020  | [Správa paměti][l7]                      |                 |
| 8.  | 12. 10. 2020 | [Bezpečnost OS][l8]                      |                 |
| 9.  | 19. 10. 2020 | [Vstup/výstup, ovladače][l9]             |                 |
| 10. | 26. 10. 2020 | [Souborové systémy][l10]                 |                 |
| 11. | 3. 11. 2020  | [Grafický subsystém, HW akcelerace][l11] |                 |
| 12. | 10. 11. 2020 | [Virtualizace][l12]                      | [Příklady][p12] |
| 13. | 17. 11. 2020 | [Mobilní OS (Android)][l13]              |                 |
| 14. | 7. 1. 2021   | [Open source, trendy v OS][l14]          |                 |

[p1]: pdf/01-priklady.tgz
[p2]: pdf/02-priklady.tgz
[p3]: pdf/03-priklady.tgz
[p4]: pdf/04-priklady.tgz
[p5]: pdf/05-priklady.tgz
[p12]: pdf/kvmtest.tar.gz

[l1]: pdf/lekce01.pdf
[l2]: pdf/lekce02.pdf
[l3]: pdf/lekce03.pdf
[l4]: pdf/lekce04.pdf
[l5]: pdf/lekce05.pdf
[l6]: pdf/lekce06.pdf
[l7]: pdf/lekce07.pdf
[l8]: pdf/osy8.pdf
[l9]: pdf/osy9.pdf
[l10]: pdf/osy10.pdf
[l11]: pdf/osy11.pdf
[l12]: pdf/lekce12_virt.pdf
[l13]: pdf/osy13.pdf
[l14]: pdf/osy14.pdf

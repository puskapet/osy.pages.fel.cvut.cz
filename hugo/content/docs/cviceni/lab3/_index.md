---
title: "3. Text, shell a regulární výrazy"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: true
---

# Zpracování textu v shellu a regulární výrazy

## Domácí příprava
Nastudujte [použití regulárních výrazů a nástroje pro zpracování textu](regex)
(alespoň zběžně `grep`, `sed` a `tr`).

# Zadání úlohy
Vytvořte skript v jazyce BASH, podle následujících požadavků:

    ...

# Materiály
- [Základy regulárních výrazů](regex)

# Domácí příprava na další cvičení
Předpokládáme, že máte základní znalosti jazyka C a víte, jak funguje překlad
ze zdrojových kódů jazyka C do binární spustitelné aplikace (v obecném případě,
kdy je zdrojových souborů více).

Dále byste měli mít alespoň minimální povědomí o použití překladače `gcc` a
jeho [základních parametrech][gcc].

Nastudujte si použití nástroje `make` pro překlad programu v jazyku C/C++:
[make][].

[gcc]: ../lab4/gcc
[make]: ../lab4/make
